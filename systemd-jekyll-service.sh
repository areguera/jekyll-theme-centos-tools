#!/bin/bash
#
# This scripts creates a rootless container with Jekyll latest version, mounts
# your Jekyll directory using volumes, and configures a systemd unit service
# for you.  This systemd unit service has two purposes:
#
#   1. Run jekyll build --watch command to rebuild the content of _site/ directory when changes happen.
#   2. Bring updates when the it is restarted using the command below:
#
#         systemctl --user restart container-${NAME}.service

NAME=${1}
SITE=${HOME}/${NAME}/

systemctl --user stop container-${NAME}.service
systemctl --user disable container-${NAME}.service

podman rm ${NAME}

podman run -d \
        --volume="${SITE}:/srv/jekyll:z" \
        --volume="${SITE}/vendor/bundle:/usr/local/bundle:z" \
        -it --name ${NAME} docker.io/jekyll/jekyll:latest \
        jekyll build --watch

pushd ${HOME}/.config/systemd/user
podman generate systemd --files --name ${NAME}
popd

podman stop ${NAME}

systemctl --user daemon-reload
systemctl --user enable container-${NAME}.service
systemctl --user start container-${NAME}.service
