#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import feedparser
import json

def main():
    parser = argparse.ArgumentParser(
        description="Convert file from RSS to JSON. Useful to maintain website \
                     sections rendered from JSON files which, in turn, must be \
                     dynamically produced from RSS files."
    )
    parser.add_argument("-i", "--input", type=str, required=True, 
                        help="Path to RSS file. It can be an absolute URL.")
    parser.add_argument("-o", "--output", type=str, required=True, 
                        help="Path to JSON file.")
    args = parser.parse_args()

    rss_file = args.input
    json_file = args.output

    json_entries = json.dumps(feedparser.parse(rss_file).entries, ensure_ascii=False)

    with open(json_file, 'w') as f:
        f.write(json_entries)

if __name__ == "__main__":
    main()
